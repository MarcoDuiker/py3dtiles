py3dtiles.tileset.extension package
===================================

py3dtiles.tileset.extension.base\_extension module
--------------------------------------------------

.. automodule:: py3dtiles.tileset.extension.base_extension
   :members:
   :show-inheritance:

py3dtiles.tileset.extension.batch\_table\_hierarchy\_extension module
---------------------------------------------------------------------

.. automodule:: py3dtiles.tileset.extension.batch_table_hierarchy_extension
   :members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py3dtiles.tileset.extension
   :members:
   :show-inheritance:
